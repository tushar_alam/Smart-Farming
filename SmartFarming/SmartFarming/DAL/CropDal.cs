﻿using SmartFarming.Models;
using SmartFarming.Models.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SmartFarming.DAL
{
    public class CropDal
    {
        FarmDbContext db = new FarmDbContext();
        bool status = false;

        internal List<Crop> List()
        {
            List<Crop> Crops = db.Crops.ToList();
            return Crops;
        }

        internal Crop GetById(int? id)
        {
            Crop Crop = db.Crops.Where(m => m.Id == id).FirstOrDefault();
            return Crop;
        }

        internal bool Edit(Crop Crop)
        {
            db.Crops.Attach(Crop);
            db.Entry(Crop).State = EntityState.Modified;
            int AffectedRow = db.SaveChanges();
            if (AffectedRow > 0)
            {
                status = true;
            }
            return status;
        }

        internal bool Delete(int id)
        {
            var CropById = db.Crops.Where(m => m.Id == id).FirstOrDefault();

            if (CropById != null)
            {
                db.Entry(CropById).State = EntityState.Deleted;
                int affectedRow = db.SaveChanges();

                if (affectedRow > 0)
                {
                    status = true;
                }
            }
            return status;
        }

        internal List<Crop> GetCropByCategoryId(int cropCategoryId)
        {
            db.Configuration.ProxyCreationEnabled = false;
            List<Crop> Crops = db.Crops.Where(m => m.CropCategoryId == cropCategoryId).ToList();
            return Crops;
        }

        internal bool Create(Crop Crop)
        {
            db.Crops.Add(Crop);
            int RowAffected = db.SaveChanges();

            if (RowAffected > 0)
            {
                status = true;
            }
            return status;
        }
    }
}