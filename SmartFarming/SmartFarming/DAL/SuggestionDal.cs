﻿using SmartFarming.Models;
using SmartFarming.Models.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SmartFarming.DAL
{
    public class SuggestionDal
    {
        FarmDbContext db = new FarmDbContext();
        bool status = false;

        internal List<Suggestion> List()
        {
            List<Suggestion> Suggestions = db.Suggestions.ToList();
            return Suggestions;
        }

        internal Suggestion GetById(int? id)
        {
            Suggestion Suggestion = db.Suggestions.Where(m => m.Id == id).FirstOrDefault();
            return Suggestion;
        }

        internal bool Edit(Suggestion Suggestion)
        {
            db.Suggestions.Attach(Suggestion);
            db.Entry(Suggestion).State = EntityState.Modified;
            int AffectedRow = db.SaveChanges();
            if (AffectedRow > 0)
            {
                status = true;
            }
            return status;
        }

        internal bool Delete(int id)
        {
            var SuggestionById = db.Suggestions.Where(m => m.Id == id).FirstOrDefault();

            if (SuggestionById != null)
            {
                db.Entry(SuggestionById).State = EntityState.Deleted;
                int affectedRow = db.SaveChanges();

                if (affectedRow > 0)
                {
                    status = true;
                }
            }
            return status;
        }

        internal bool Create(Suggestion Suggestion)
        {
            db.Suggestions.Add(Suggestion);
            int RowAffected = db.SaveChanges();

            if (RowAffected > 0)
            {
                status = true;
            }
            return status;
        }
    }
}