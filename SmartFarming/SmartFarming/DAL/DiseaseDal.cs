﻿using SmartFarming.Models;
using SmartFarming.Models.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SmartFarming.DAL
{
    public class DiseaseDal
    {
        FarmDbContext db = new FarmDbContext();
        bool status = false;

        internal List<Disease> List()
        {
            List<Disease> Diseases = db.Diseases.ToList();
            return Diseases;
        }

        internal Disease GetById(int? id)
        {
            Disease Disease = db.Diseases.Where(m => m.Id == id).FirstOrDefault();
            return Disease;
        }

        internal bool Edit(Disease Disease)
        {
            db.Diseases.Attach(Disease);
            db.Entry(Disease).State = EntityState.Modified;
            int AffectedRow = db.SaveChanges();
            if (AffectedRow > 0)
            {
                status = true;
            }
            return status;
        }

        internal bool Delete(int id)
        {
            var DiseaseById = db.Diseases.Where(m => m.Id == id).FirstOrDefault();

            if (DiseaseById != null)
            {
                db.Entry(DiseaseById).State = EntityState.Deleted;
                int affectedRow = db.SaveChanges();

                if (affectedRow > 0)
                {
                    status = true;
                }
            }
            return status;
        }

        internal bool Create(Disease Disease)
        {
            db.Diseases.Add(Disease);
            int RowAffected = db.SaveChanges();

            if (RowAffected > 0)
            {
                status = true;
            }
            return status;
        }
    }
}