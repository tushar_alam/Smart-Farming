﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using SmartFarming.Models;
using SmartFarming.Models.Context;
using SmartFarming.Models.ViewModels;

namespace SmartFarming.DAL
{
    public class AccountDal
    {
        Account account = new Account();
        FarmDbContext db = new FarmDbContext();
        bool status=false;
        internal Account Registration(Account account )
        {
            db.Accounts.Add(account);
            int affectedRow = db.SaveChanges();

            if (affectedRow>0)
            {
                account = db.Accounts.Where(m => m.AccountId == account.AccountId).FirstOrDefault();
            }
            return account;
        }

        internal Account GetAccount(LoginVm loginVm)
        {
            account = db.Accounts.Where(m => m.EmailOrPhone == loginVm.EmailOrPhone && m.Password == loginVm.Pass).FirstOrDefault();
            return account;
        }

        internal bool CheckExistance(RegistrationVm registrationVm)
        {
            account = db.Accounts.Where(m=>m.EmailOrPhone==registrationVm.EmailOrPhone).FirstOrDefault();

            if (account!=null)
            {
                status = true;
            }
            return status;
        }

        internal List<Account> List()
        {
            List<Account> Accounts = db.Accounts.ToList();
            return Accounts;
        }

        internal Account GetById(int? id)
        {
            Account Accounts = db.Accounts.Where(m => m.AccountId == id).FirstOrDefault();
            return Accounts;
        }

        internal bool Edit(Account account)
        {
            db.Accounts.Attach(account);
            db.Entry(account).State = EntityState.Modified;
            int AffectedRow = db.SaveChanges();
            if (AffectedRow > 0)
            {
                status = true;
            }
            return status;
        }

        internal bool Delete(int id)
        {
            var AccountById = db.Accounts.Where(m => m.AccountId == id).FirstOrDefault();

            if (AccountById != null)
            {
                db.Entry(AccountById).State = EntityState.Deleted;
                int affectedRow = db.SaveChanges();

                if (affectedRow > 0)
                {
                    status = true;
                }
            }
            return status;
        }
        internal bool Create(Account account)
        {
            db.Accounts.Add(account);
            int RowAffected = db.SaveChanges();

            if (RowAffected > 0)
            {
                status = true;
            }
            return status;
        }
    }
}