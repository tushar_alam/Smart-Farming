﻿using SmartFarming.Models;
using SmartFarming.Models.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SmartFarming.DAL
{
    public class ChatDal
    {
        FarmDbContext db = new FarmDbContext();
        bool status = false;

        internal List<Chat> List()
        {
            List<Chat> Chats = db.Chats.ToList();
            return Chats;
        }

        internal Chat GetById(int? id)
        {
            Chat Chat = db.Chats.Where(m => m.Id == id).FirstOrDefault();
            return Chat;
        }

        internal bool Edit(Chat Chat)
        {
            db.Chats.Attach(Chat);
            db.Entry(Chat).State = EntityState.Modified;
            int AffectedRow = db.SaveChanges();
            if (AffectedRow > 0)
            {
                status = true;
            }
            return status;
        }

        internal bool Delete(int id)
        {
            var ChatById = db.Chats.Where(m => m.Id == id).FirstOrDefault();

            if (ChatById != null)
            {
                db.Entry(ChatById).State = EntityState.Deleted;
                int affectedRow = db.SaveChanges();

                if (affectedRow > 0)
                {
                    status = true;
                }
            }
            return status;
        }

        internal List<Chat> ListById(int? id)
        {
            List<Chat> Chats = db.Chats.Where(m => m.FarmerId == id).ToList();
            return Chats;
        }

        internal bool Create(Chat Chat)
        {
            db.Chats.Add(Chat);
            int RowAffected = db.SaveChanges();

            if (RowAffected > 0)
            {
                status = true;
            }
            return status;
        }
    }
}