﻿using SmartFarming.Models;
using SmartFarming.Models.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SmartFarming.DAL
{
    public class CropCategoryDal
    {
        FarmDbContext db = new FarmDbContext();
        bool status = false;

        internal List<CropCategory> List()
        {
            List<CropCategory> CropCategories = db.CropCategories.ToList();
            return CropCategories;
        }

        internal CropCategory GetById(int? id)
        {
            CropCategory CropCategory = db.CropCategories.Where(m => m.Id == id).FirstOrDefault();
            return CropCategory;
        }

        internal bool Edit(CropCategory CropCategory)
        {
            db.CropCategories.Attach(CropCategory);
            db.Entry(CropCategory).State = EntityState.Modified;
            int AffectedRow = db.SaveChanges();
            if (AffectedRow > 0)
            {
                status = true;
            }
            return status;
        }

        internal bool Delete(int id)
        {
            var CropCategoryById = db.CropCategories.Where(m => m.Id == id).FirstOrDefault();

            if (CropCategoryById != null)
            {
                db.Entry(CropCategoryById).State = EntityState.Deleted;
                int affectedRow = db.SaveChanges();

                if (affectedRow > 0)
                {
                    status = true;
                }
            }
            return status;
        }

        internal bool Create(CropCategory CropCategory)
        {
            db.CropCategories.Add(CropCategory);
            int RowAffected = db.SaveChanges();

            if (RowAffected > 0)
            {
                status = true;
            }
            return status;
        }
    }
}