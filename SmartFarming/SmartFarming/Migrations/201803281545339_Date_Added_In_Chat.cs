namespace SmartFarming.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Date_Added_In_Chat : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Chats", "Date", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Chats", "Date");
        }
    }
}
