namespace SmartFarming.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Image_Added_In_Account : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Accounts", "Image", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Accounts", "Image");
        }
    }
}
