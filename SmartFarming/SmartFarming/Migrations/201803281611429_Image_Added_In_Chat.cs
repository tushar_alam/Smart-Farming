namespace SmartFarming.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Image_Added_In_Chat : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Chats", "Image", c => c.Binary());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Chats", "Image");
        }
    }
}
