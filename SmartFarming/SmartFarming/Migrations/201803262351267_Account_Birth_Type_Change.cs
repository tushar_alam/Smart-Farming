namespace SmartFarming.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Account_Birth_Type_Change : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Accounts", "DateOfBirth", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Accounts", "DateOfBirth", c => c.DateTime(nullable: false));
        }
    }
}
