namespace SmartFarming.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CropCategory_Disease_Linked : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Diseases", "CropId", "dbo.Crops");
            AddColumn("dbo.Diseases", "CropCategoryId", c => c.Int(nullable: false));
            CreateIndex("dbo.Diseases", "CropCategoryId");
            AddForeignKey("dbo.Diseases", "CropCategoryId", "dbo.CropCategories", "Id", cascadeDelete: true);
            AddForeignKey("dbo.Diseases", "CropId", "dbo.Crops", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Diseases", "CropId", "dbo.Crops");
            DropForeignKey("dbo.Diseases", "CropCategoryId", "dbo.CropCategories");
            DropIndex("dbo.Diseases", new[] { "CropCategoryId" });
            DropColumn("dbo.Diseases", "CropCategoryId");
            AddForeignKey("dbo.Diseases", "CropId", "dbo.Crops", "Id", cascadeDelete: true);
        }
    }
}
