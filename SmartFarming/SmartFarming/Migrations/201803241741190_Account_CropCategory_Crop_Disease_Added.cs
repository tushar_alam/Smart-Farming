namespace SmartFarming.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Account_CropCategory_Crop_Disease_Added : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountId = c.Int(nullable: false, identity: true),
                        EmailOrPhone = c.String(nullable: false),
                        Password = c.String(nullable: false),
                        Name = c.String(nullable: false),
                        Gander = c.String(),
                        Profession = c.String(),
                        DateOfBirth = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.AccountId);
            
            CreateTable(
                "dbo.CropCategories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(maxLength: 1000),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Crops",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CropCategoryId = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Description = c.String(maxLength: 1000),
                        Image = c.Binary(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CropCategories", t => t.CropCategoryId, cascadeDelete: true)
                .Index(t => t.CropCategoryId);
            
            CreateTable(
                "dbo.Diseases",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        CropId = c.Int(nullable: false),
                        Name = c.String(nullable: false),
                        Symptom = c.String(nullable: false),
                        Management = c.String(nullable: false),
                        AfterWork = c.String(nullable: false),
                        Image = c.Binary(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Crops", t => t.CropId, cascadeDelete: true)
                .Index(t => t.CropId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Diseases", "CropId", "dbo.Crops");
            DropForeignKey("dbo.Crops", "CropCategoryId", "dbo.CropCategories");
            DropIndex("dbo.Diseases", new[] { "CropId" });
            DropIndex("dbo.Crops", new[] { "CropCategoryId" });
            DropTable("dbo.Diseases");
            DropTable("dbo.Crops");
            DropTable("dbo.CropCategories");
            DropTable("dbo.Accounts");
        }
    }
}
