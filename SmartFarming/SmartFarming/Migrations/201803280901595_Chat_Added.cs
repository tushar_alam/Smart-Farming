namespace SmartFarming.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Chat_Added : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Chats",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Question = c.String(maxLength: 1000),
                        Answer = c.String(),
                        OfficerId = c.Int(nullable: false),
                        FarmerId = c.Int(nullable: false),
                        Farmer_AccountId = c.Int(),
                        Officer_AccountId = c.Int(),
                        Account_AccountId = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.Farmer_AccountId)
                .ForeignKey("dbo.Accounts", t => t.Officer_AccountId)
                .ForeignKey("dbo.Accounts", t => t.Account_AccountId)
                .Index(t => t.Farmer_AccountId)
                .Index(t => t.Officer_AccountId)
                .Index(t => t.Account_AccountId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Chats", "Account_AccountId", "dbo.Accounts");
            DropForeignKey("dbo.Chats", "Officer_AccountId", "dbo.Accounts");
            DropForeignKey("dbo.Chats", "Farmer_AccountId", "dbo.Accounts");
            DropIndex("dbo.Chats", new[] { "Account_AccountId" });
            DropIndex("dbo.Chats", new[] { "Officer_AccountId" });
            DropIndex("dbo.Chats", new[] { "Farmer_AccountId" });
            DropTable("dbo.Chats");
        }
    }
}
