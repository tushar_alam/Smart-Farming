﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartFarming.Models
{
    public class Disease
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "ফসলের শ্রেণী বা বিভাগ এর নাম নির্বাচন করুন")]
        [Display(Name = "ফসলের ক্যাটাগরি")]
        public int CropCategoryId { get; set; }
        public virtual CropCategory CropCategory { get; set; }

        [Required(ErrorMessage = "ফসলের এর নাম নির্বাচন করুন")]
        [Display(Name = "ফসলের নামঃ")]
        public int CropId { get; set; }
        public virtual Crop Crop { get; set; }


        [Required(ErrorMessage = "রোগের এর নাম প্রদান করুন")]
        [Display(Name = "রোগের নামঃ")]
        public string Name { get; set; }

        [Required(ErrorMessage = "লক্ষণগুলো প্রদান করুন")]
        [Display(Name = "লক্ষনঃ")]
        [DataType(DataType.MultilineText)]
        public string Symptom { get; set; }

        [Required(ErrorMessage = "বেবস্থাপনা সমূহ প্রদান করুন")]
        [Display(Name = "ব্যবস্থাপনাঃ")]
        [DataType(DataType.MultilineText)]
        public string Management { get; set; }

        [Required(ErrorMessage = "পরবর্তীতে করনীয় সমূহ প্রদান করুন")]
        [Display(Name = "পরবর্তীতে যা যা করবেনঃ")]
        [DataType(DataType.MultilineText)]
        public string AfterWork { get; set; }

        [Display(Name ="রোগাক্রান্ত ফসলের ছবিঃ")]
        public byte[] Image { get; set; }
        
    }
}