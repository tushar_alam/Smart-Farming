﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartFarming.Models
{
    public class Account
    {
        public int AccountId { get; set; }

        [Required(ErrorMessage = "আপনার ইমেইল অথবা ফোন নম্বর প্রদান করুন")]
        [Display(Name = "ইমেইল অথবা ফোন")]
        public string EmailOrPhone { get; set; }

        [Required(ErrorMessage = "পাসওয়ার্ড প্রদান করুন")]
        [Display(Name = "পাসওয়ার্ড")]
        public string Password { get; set; }

        [Required(ErrorMessage = "আপনার নাম প্রদান করুন")]
        [Display(Name = "সম্পূর্ণ নাম")]
        public string Name { get; set; }

        [Display(Name = "লিঙ্গ")]
        public string Gander { get; set; }

        [Display(Name = "পেশা ")]
        public string Profession { get; set; }

        [Required(ErrorMessage = "আপনার জন্ম তারিখ প্রদান করুন")]
        [Display(Name = "জম্ন তারিখ")]
        public string DateOfBirth { get; set; }

        [Display(Name = "ছবি")]
        public byte[] Image { get; set; }

        public virtual List<Suggestion> Suggestions { get; set; }
        public virtual List<Chat> Chats { get; set; }
    }
}