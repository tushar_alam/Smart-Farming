﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SmartFarming.Models.Context
{
    public class FarmDbContext:DbContext
    {
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Crop> Crops { get; set; }
        public DbSet<CropCategory> CropCategories { get; set; }
        public DbSet<Disease> Diseases { get; set; }
        public DbSet<Suggestion> Suggestions { get; set; }
        public DbSet<Chat> Chats { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Disease>()
                .HasRequired(d => d.Crop)
                .WithMany(w => w.Diseases)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Suggestion>()
             .HasRequired(d => d.Crop)
             .WithMany(w => w.Suggestions)
             .WillCascadeOnDelete(false);

            modelBuilder.Entity<Suggestion>()
             .HasRequired(d => d.CropCategory)
             .WithMany(w => w.Suggestions)
             .WillCascadeOnDelete(false);
            
            base.OnModelCreating(modelBuilder);
        }
    }
}