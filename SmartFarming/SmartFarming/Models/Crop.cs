﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartFarming.Models
{
    public class Crop
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "ফসলের শ্রেণী বা বিভাগ এর নাম নির্বাচন করুন")]
        [Display(Name = "ফসলের ক্যাটাগরি")]
        public int CropCategoryId { get; set; }
        public virtual CropCategory CropCategory { get; set; }

        [Required(ErrorMessage = "ফসলের এর নাম প্রদান করুন")]
        [Display(Name ="ফসলের নাম")]
        public string Name { get; set; }

        [Display(Name = "বর্ণনা")]
        [DataType(DataType.MultilineText)]
        [StringLength(maximumLength: 1000, ErrorMessage = "বর্ণনাটি ১০০০ বর্ণের অধিক হতে পারবে না।")]
        public string Description { get; set; }

        [Display(Name = "ছবি")]
        public byte[] Image { get; set; }

        public virtual List<Disease> Diseases { get; set; }
        public virtual List<Suggestion> Suggestions { get; set; }
    }
}