﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartFarming.Models
{
    public class Chat
    {
        [Key]
        public int Id { get; set; }
        
        [Display(Name = "প্রশ্ন")]
        [StringLength(maximumLength: 1000, ErrorMessage = "প্রশ্নটি ১০০০ বর্ণের অধিক হতে পারবে না।")]
        public string  Question { get; set; }

        [Display(Name = "প্রশ্ন")]
        [DataType(DataType.MultilineText)]
        public string Answer { get; set; }

        [Display(Name = "আজকের তারিখ")]
        public string Date { get; set; }

        [Display(Name = "আজকের তারিখ")]
        public byte[] Image { get; set; }

        public int OfficerId { get; set; }
        public Account Officer { get; set; }

        public int FarmerId { get; set; }
        public Account Farmer { get; set; }
    }
}