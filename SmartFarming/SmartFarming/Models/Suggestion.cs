﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartFarming.Models
{
    public class Suggestion
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "ফসলের শ্রেণী বা বিভাগ এর নাম নির্বাচন করুন")]
        [Display(Name = "ফসলের ক্যাটাগরি")]
        public int CropCategoryId { get; set; }
        public virtual CropCategory CropCategory { set; get; }

        [Required(ErrorMessage = "ফসলের নাম নির্বাচন করুন")]
        [Display(Name = "ফসলের নাম")]
        public int CropId { set; get; }
        public virtual Crop Crop { set; get; }
        
        [Required(ErrorMessage = "সাজেশনটি বর্ণনা করুন")]
        [Display(Name = "সাজেশন")]
        public string Description { get; set; }

        public string Date { set; get; }

        public int AccountId { get; set; }
        public virtual Account Account { get; set; }
    }
}