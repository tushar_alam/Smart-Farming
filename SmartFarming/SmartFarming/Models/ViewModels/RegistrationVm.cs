﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartFarming.Models.ViewModels
{
    public class RegistrationVm
    {
        [Required(ErrorMessage = "আপনার নাম প্রদান করুন")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "আপনার ইমেইল অথবা ফোন নম্বর প্রদান করুন")]
        public string EmailOrPhone { get; set; }

        [Required(ErrorMessage = "একটি পাসওয়ার্ড প্রদান করুন")]
        public string Pass { get; set; }

        [Required(ErrorMessage = "পাসওয়ার্ডটি পুনরায় প্রদান করুন")]
        public string RePass { get; set; }

        [Required(ErrorMessage = "আপনার লিঙ্গ নির্বাচন করুন")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "আপনার পেষা নির্বাচন করুন")]
        public string Occupation { get; set; }

        public string BirthDay { get; set; }
    }
}