﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SmartFarming.Models.ViewModels
{
    public class LoginVm
    {
        [Required(ErrorMessage = "আপনার ইমেইল অথবা ফোন নম্বর প্রদান করুন")]
        public string EmailOrPhone { get; set; }

        [Required(ErrorMessage = "আপনার পাসওয়ার্ড প্রদান করুন")]
        public string Pass { get; set; }
    }
}