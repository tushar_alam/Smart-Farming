﻿using SmartFarming.BLL;
using SmartFarming.Models;
using SmartFarming.Models.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartFarming.Controllers
{
    public class SuggestionsController : Controller
    {
        FarmDbContext db = new FarmDbContext();
        SuggestionBll suggestionBll = new SuggestionBll();
        bool status;

        // GET: Suggestions
        public ActionResult List()
        {
            List<Suggestion> Suggestions = suggestionBll.List();
            return View(Suggestions);
        }
        public ActionResult ListFarmer()
        {
            List<Suggestion> Suggestions = suggestionBll.List();
            return View(Suggestions);
        }
        public ActionResult ListCommon()
        {
            List<Suggestion> Suggestions = suggestionBll.List();
            return View(Suggestions);
        }

        // GET: Suggestions/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        // GET: Suggestions/Create
        public ActionResult Create()
        {
            ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
            return View();
        }

        // POST: Suggestions/Create
        [HttpPost]
        public ActionResult Create(Suggestion suggestion)
        {
            if (Session["AccountId"] != null)
            {
                suggestion.AccountId =Convert.ToInt32( Session["AccountId"].ToString());
            }
            else
            {
                RedirectToAction("LogIn","Accounts");
            }
            if (suggestion.CropCategoryId!=null && suggestion.CropId!=null && suggestion.Description!=null && suggestion.AccountId!=null)
            {
                status = suggestionBll.Create(suggestion);
                if (status == true)
                {
                    return RedirectToAction("List");
                }
                else
                {
                    ViewBag.Message = "আপনার পরামর্শ যুক্ত হয়নি";
                }
            }

            ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
            return View(suggestion);
        }

        // GET: Suggestions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Suggestion suggestion = suggestionBll.GetById(id);
            if (suggestion == null)
            {
                return RedirectToAction("Error", "Home");
            }
            ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
            return View(suggestion);
        }

        // POST: Suggestions/Edit/5
        [HttpPost]
        public ActionResult Edit(Suggestion suggestion)
        {
            if (Session["AccountId"] != null)
            {
                suggestion.AccountId = Convert.ToInt32(Session["AccountId"].ToString());
            }
            else
            {
                RedirectToAction("LogIn", "Accounts");
            }
            if (ModelState.IsValid)
            {
                status = suggestionBll.Edit(suggestion);
                if (status == true)
                {
                    return RedirectToAction("List", "Suggestions");
                }
                else
                {
                    ViewBag.Message = "আপনার পরামর্শ পরিবর্তিত হয়নি";
                }
            }
            ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
            return View(suggestion);
        }

        // GET: Suggestions/Delete/5
        public JsonResult Delete(int id)
        {
            status = suggestionBll.Delete(id);
            if (status == true)
            {
                return Json(1);
            }
            return Json(0);
        }
    }
}