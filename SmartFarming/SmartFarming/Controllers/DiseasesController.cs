﻿using Rotativa;
using SmartFarming.BLL;
using SmartFarming.Models;
using SmartFarming.Models.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartFarming.Controllers
{
    public class DiseasesController : Controller
    {
        FarmDbContext db = new FarmDbContext();
        DiseaseBll diseaseBll = new DiseaseBll();
        CropBll cropBll = new CropBll();
        Common common = new Common();
        bool status;

        // GET: Diseases
        public ActionResult List()
        {
            List<Disease> Diseases = diseaseBll.List();
            return View(Diseases);
        }
        public ActionResult ListFarmer()
        {
            List<Disease> Diseases = diseaseBll.List();
            return View(Diseases);
        }

        // GET: Diseases/Details/5
        public ActionResult Details(int id)
        {
            Disease disease = diseaseBll.GetById(id);
            return View(disease);
        }
        public ActionResult DetailsFarmer(int id)
        {
            Disease disease = diseaseBll.GetById(id);
            return View(disease);
        }

        public ActionResult DetailsPdf(int id)
        {
            Disease disease = diseaseBll.GetById(id);
            return View(disease);
        }

        public ActionResult ExportPdf(int id)
        {
            return new ActionAsPdf("DetailsPdf", new { id = id });
        }

        // GET: Diseases/Create
        public ActionResult Create()
        {
            ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
            //ViewBag.CropId = new SelectList(db.Crops, "Id", "Name");
            return View();
        }

        // POST: Diseases/Create
        [HttpPost]
        public ActionResult Create(Disease disease, HttpPostedFileBase ImageFile)
        {
            if (ImageFile == null)
            {
                ModelState.AddModelError("Image", "দয়া করে একটি ছবি আপলোড করুন");
                ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
                // ViewBag.CropId = new SelectList(db.Crops, "Id", "Name");
                return View(disease);
            }

            bool IsValidFormat = common.ImageValidation(ImageFile);


            if (IsValidFormat == false)
            {
                ModelState.AddModelError("Image", "শুধুমাত্র jpg, png, jpeg ফরম্যাট এর ছবিগুলই গ্রহণযোগ্য ");
                ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
                // ViewBag.CropId = new SelectList(db.Crops, "Id", "Name");
                return View(disease);
            }
            byte[] convertedImage = common.ConvertImage(ImageFile);
            disease.Image = convertedImage;

            if (ModelState.IsValid)
            {
                status = diseaseBll.Create(disease);
                if (status == true)
                {
                    return RedirectToAction("List");
                }
                else
                {
                    ViewBag.Message = "দুঃখিত রোগটি যুক্ত হয়নি";
                }
            }
            ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
           // ViewBag.CropId = new SelectList(db.Crops, "Id", "Name");
            return View(disease);
        }

        // GET: Diseases/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Disease disease = diseaseBll.GetById(id);
            if (disease == null)
            {
                return RedirectToAction("Error", "Home");
            }
            ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
            //ViewBag.CropId = new SelectList(db.Crops, "Id", "Name");
            return View(disease);
        }

        // POST: Diseases/Edit/5
        [HttpPost]
        public ActionResult Edit(Disease disease, HttpPostedFileBase ImageFile)
        {
            if (ImageFile != null)
            {
                bool IsValidFormate = common.ImageValidation(ImageFile);
                if (IsValidFormate == false)
                {
                    ModelState.AddModelError("Image", "শুধুমাত্র jpg, png, jpeg ফরম্যাট এর ছবিগুলই গ্রহণযোগ্য ");
                    ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
                    //ViewBag.CropId = new SelectList(db.Crops, "Id", "Name");
                    return View(disease);
                }
                byte[] CurrentImage = common.ConvertImage(ImageFile);
                disease.Image = CurrentImage;
            }
            if (ModelState.IsValid)
            {
                status = diseaseBll.Edit(disease);
                if (status == true)
                {
                    return RedirectToAction("List", "Diseases");
                }
                else
                {
                    ViewBag.Message = "রোগটির বর্ণনায় কোন পরিবর্তন হয়নি";
                }
            }
            ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
            //ViewBag.CropId = new SelectList(db.Crops, "Id", "Name");
            return View(disease);
        }

        public JsonResult GetCropByCategoryId(int id)
        {
            var Crops =cropBll.GetCropByCategoryId(id);
            return Json(Crops,JsonRequestBehavior.AllowGet);
        }

        // GET: Diseases/Delete/5
        public JsonResult Delete(int id)
        {
            status = diseaseBll.Delete(id);

            if (status == true)
            {
                return Json(1);
            }
            return Json(0);
        }
    }
}