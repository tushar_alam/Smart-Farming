﻿using SmartFarming.BLL;
using SmartFarming.Models;
using SmartFarming.Models.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartFarming.Controllers
{
    public class CropCategoriesController : Controller
    {
        FarmDbContext db = new FarmDbContext();
        CropCategoryBll cropCategoryBll = new CropCategoryBll();
        bool status;

        // GET: CropCategories
        public ActionResult List()
        {
            List<CropCategory> CropCategories = cropCategoryBll.List();
            return View(CropCategories);
        }
        public ActionResult ListAdmin()
        {
            List<CropCategory> CropCategories = cropCategoryBll.List();
            return View(CropCategories);
        }

        // GET: CropCategories/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        // GET: CropCategories/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: CropCategories/Create
        [HttpPost]
        public ActionResult Create(CropCategory cropCategory)
        {
            if (ModelState.IsValid)
            {
                status = cropCategoryBll.Create(cropCategory);
                if (status == true)
                {
                    return RedirectToAction("List");
                }
                else
                {
                    ViewBag.Message = "ফসলের শ্রেণী বা বিভাগ যুক্ত হয়নি";
                }
            }
            return View(cropCategory);
        }

        public ActionResult CreateAdmin()
        {
            return View();
        }

        // POST: CropCategories/Create
        [HttpPost]
        public ActionResult CreateAdmin(CropCategory cropCategory)
        {
            if (ModelState.IsValid)
            {
                status = cropCategoryBll.Create(cropCategory);
                if (status == true)
                {
                    return RedirectToAction("List");
                }
                else
                {
                    ViewBag.Message = "ফসলের শ্রেণী বা বিভাগ যুক্ত হয়নি";
                }
            }
            return View(cropCategory);
        }

        // GET: CropCategories/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            CropCategory cropCategory = cropCategoryBll.GetById(id);
            if (cropCategory == null)
            {
                return RedirectToAction("Error", "Home");
            }
            return View(cropCategory);
        }

        // POST: CropCategories/Edit/5
        [HttpPost]
        public ActionResult Edit(CropCategory cropCategory)
        {
            if (ModelState.IsValid)
            {
                status = cropCategoryBll.Edit(cropCategory);
                if (status == true)
                {
                    return RedirectToAction("List", "CropCategories");
                }
                else
                {
                    ViewBag.Message = "ফসলের শ্রেণী বা বিভাগটি পরিবর্তিত হয়নি";
                }
            }
            return View(cropCategory);
        }

        public ActionResult EditAdmin(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            CropCategory cropCategory = cropCategoryBll.GetById(id);
            if (cropCategory == null)
            {
                return RedirectToAction("Error", "Home");
            }
            return View(cropCategory);
        }

        // POST: CropCategories/Edit/5
        [HttpPost]
        public ActionResult EditAdmin(CropCategory cropCategory)
        {
            if (ModelState.IsValid)
            {
                status = cropCategoryBll.Edit(cropCategory);
                if (status == true)
                {
                    return RedirectToAction("List", "CropCategories");
                }
                else
                {
                    ViewBag.Message = "ফসলের শ্রেণী বা বিভাগটি পরিবর্তিত হয়নি";
                }
            }
            return View(cropCategory);
        }

        // GET: CropCategories/Delete/5
        public JsonResult Delete(int id)
        {
            status = cropCategoryBll.Delete(id);

            if (status == true)
            {
                return Json(1);
            }

            return Json(0);
        }
    }
}
