﻿using SmartFarming.BLL;
using SmartFarming.Models;
using SmartFarming.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartFarming.Controllers
{
    public class AccountsController : Controller
    {
        Common common = new Common();
        Account account = new Account();
        AccountBll accountBll = new AccountBll();
        bool status=false;
        bool isExist = false;
        // GET: Accounts
        //public ActionResult Index()
        //{
        //    return View();
        //}
        // GET: Accounts/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        public ActionResult LogIn()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LogIn(LoginVm loginVm)
        {
            if (ModelState.IsValid)
            {
                account = accountBll.GetAccount(loginVm);
                if (account!=null)
                {
                    if (account.Profession == "কৃষি কর্মকর্তা")
                    {
                        Session["AccountId"] = account.AccountId;
                        return RedirectToAction("Create", "CropCategories");
                    }
                    if (account.Profession == "কৃষক")
                    {
                        Session["AccountId"] = account.AccountId;
                        return RedirectToAction("ListFarmer", "Diseases");
                    }
                    if (account.Profession == "এডমিন")
                    {
                        Session["AccountId"] = account.AccountId;
                        return RedirectToAction("Create", "Accounts");
                    }
                }
                else
                {
                    ViewBag.Message = "দুঃখিত!ভুল পাসওয়ার্ড অথবা আইডি প্রবেশ করানো হয়েছে।";
                }
            }
            return View(loginVm);
        }
        // GET: Accounts/Registration
        public ActionResult Registration()
        {
            return View();
        }
        // POST: Accounts/Registration
        [HttpPost]
        public ActionResult Registration(RegistrationVm registrationVm)
        {
            if (registrationVm.Pass!=registrationVm.RePass)
            {
                ModelState.AddModelError("RePass", "পাসওয়ার্ডটি আনুরুপ হয়নি");
                return View(registrationVm);
            }

            if (ModelState.IsValid)
            {

                isExist = accountBll.CheckExistance(registrationVm);
                if(isExist==false)
                {
                    account = accountBll.Registration(registrationVm);
                    if (account!=null)
                    {
                        if (registrationVm.Occupation == "কৃষি কর্মকর্তা")
                        {
                            Session["AccountId"] = account.AccountId;
                            return RedirectToAction("Create", "CropCategories");
                        }
                        if (registrationVm.Occupation == "কৃষক")
                        {
                            Session["AccountId"] = account.AccountId;
                            return RedirectToAction("ListFarmer", "Diseases");
                        }
                    }
                    else
                    {
                        ViewBag.Message = "দুঃখিত আপনার একাউন্টটি রেজিসট্রেসন হয়নি";
                    }
                }
                else
                {
                    ViewBag.Message = "দুঃখিত! অনুরূপ ইমেইল অথবা ফোন নম্বরের একাউন্ট রয়েছে";
                }
            }
            return View(registrationVm);
        }

        public ActionResult List()
        {
            List<Account> Accounts = accountBll.List();
            return View(Accounts);
        }

        // GET: Crops/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        // GET: Crops/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Crops/Create
        [HttpPost]
        public ActionResult Create(Account account, HttpPostedFileBase ImageFile)
        {
            if (ImageFile == null)
            {
                ModelState.AddModelError("Image", "দয়া করে একটি ছবি আপলোড করুন");
                return View(account);
            }

            bool IsValidFormat = common.ImageValidation(ImageFile);


            if (IsValidFormat == false)
            {
                ModelState.AddModelError("Image", "শুধুমাত্র jpg, png, jpeg ফরম্যাট এর ছবিগুলই গ্রহণযোগ্য ");
                return View(account);
            }
            byte[] convertedImage = common.ConvertImage(ImageFile);
            account.Image = convertedImage;

            if (ModelState.IsValid)
            {
                status = accountBll.Create(account);
                if (status == true)
                {
                    return RedirectToAction("List");
                }
                else
                {
                    ViewBag.Message = "দুঃখিত তথ্যটি যুক্ত হয়নি";
                }
            }
            
            return View(account);
        }

        // GET: Crops/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Account account = accountBll.GetById(id);
            if (account == null)
            {
                return RedirectToAction("Error", "Home");
            }
            
            return View(account);
        }

        // POST: Crops/Edit/5
        [HttpPost]
        public ActionResult Edit(Account account, HttpPostedFileBase ImageFile)
        {
            if (ImageFile != null)
            {
                bool IsValidFormate = common.ImageValidation(ImageFile);
                if (IsValidFormate == false)
                {
                    ModelState.AddModelError("Image", "শুধুমাত্র jpg, png, jpeg ফরম্যাট এর ছবিগুলই গ্রহণযোগ্য ");
                    return View(account);
                }
                byte[] CurrentImage = common.ConvertImage(ImageFile);
                account.Image = CurrentImage;
            }
            if (ModelState.IsValid)
            {
                status = accountBll.Edit(account);
                if (status == true)
                {
                    return RedirectToAction("List", "Accounts");
                }
                else
                {
                    ViewBag.Message = "তথ্যটির বর্ণনায় কোন পরিবর্তন হয়নি";
                }
            }

            return View(account);
        }

        // GET: Crops/Delete/5
        public JsonResult Delete(int id)
        {
            status = accountBll.Delete(id);

            if (status == true)
            {
                return Json(1);
            }
            return Json(0);
        }
    }
}
