﻿using SmartFarming.BLL;
using SmartFarming.Models;
using SmartFarming.Models.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartFarming.Controllers
{
    public class CropsController : Controller
    {
        FarmDbContext db = new FarmDbContext();
        CropBll cropBll = new CropBll();
        Common common = new Common();
        bool status;

        // GET: Crops
        public ActionResult List()
        {
            List<Crop> Crops = cropBll.List();
            return View(Crops);
        }
        public ActionResult ListAdmin()
        {
            List<Crop> Crops = cropBll.List();
            return View(Crops);
        }
        // GET: Crops/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        // GET: Crops/Create
        public ActionResult Create()
        {
            ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
            return View();
        }

        // POST: Crops/Create
        [HttpPost]
        public ActionResult Create(Crop crop, HttpPostedFileBase ImageFile)
        {
            if (ImageFile == null)
            {
                ModelState.AddModelError("Image", "দয়া করে একটি ছবি আপলোড করুন");
                ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
                return View(crop);
            }
            bool IsValidFormat = common.ImageValidation(ImageFile);

            if (IsValidFormat == false)
            {
                ModelState.AddModelError("Image", "শুধুমাত্র jpg, png, jpeg ফরম্যাট এর ছবিগুলই গ্রহণযোগ্য ");
                ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
                return View(crop);
            }
            byte[] convertedImage = common.ConvertImage(ImageFile);
            crop.Image = convertedImage;

            if (ModelState.IsValid)
            {
                status = cropBll.Create(crop);
                if (status == true)
                {
                    return RedirectToAction("List");
                }
                else
                {
                    ViewBag.Message = "দুঃখিত ফসলটি যুক্ত হয়নি";
                }
            }
            ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
            return View(crop);
        }

        public ActionResult CreateAdmin()
        {
            ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
            return View();
        }

        // POST: Crops/Create
        [HttpPost]
        public ActionResult CreateAdmin(Crop crop, HttpPostedFileBase ImageFile)
        {
            if (ImageFile == null)
            {
                ModelState.AddModelError("Image", "দয়া করে একটি ছবি আপলোড করুন");
                ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
                return View(crop);
            }
            bool IsValidFormat = common.ImageValidation(ImageFile);

            if (IsValidFormat == false)
            {
                ModelState.AddModelError("Image", "শুধুমাত্র jpg, png, jpeg ফরম্যাট এর ছবিগুলই গ্রহণযোগ্য ");
                ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
                return View(crop);
            }
            byte[] convertedImage = common.ConvertImage(ImageFile);
            crop.Image = convertedImage;

            if (ModelState.IsValid)
            {
                status = cropBll.Create(crop);
                if (status == true)
                {
                    return RedirectToAction("List");
                }
                else
                {
                    ViewBag.Message = "দুঃখিত ফসলটি যুক্ত হয়নি";
                }
            }
            ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
            return View(crop);
        }
        // GET: Crops/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Crop crop = cropBll.GetById(id);
            if (crop == null)
            {
                return RedirectToAction("Error", "Home");
            }
            ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
            return View(crop);
        }

        // POST: Crops/Edit/5
        [HttpPost]
        public ActionResult Edit(Crop crop, HttpPostedFileBase ImageFile)
        {
            if (ImageFile != null)
            {
                bool IsValidFormate = common.ImageValidation(ImageFile);
                if (IsValidFormate == false)
                {
                    ModelState.AddModelError("Image", "শুধুমাত্র jpg, png, jpeg ফরম্যাট এর ছবিগুলই গ্রহণযোগ্য ");
                    ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
                    return View(crop);
                }
                byte[] CurrentImage = common.ConvertImage(ImageFile);
                crop.Image = CurrentImage;
            }
            if (ModelState.IsValid)
            {
                status = cropBll.Edit(crop);
                if (status == true)
                {
                    return RedirectToAction("List", "Crops");
                }
                else
                {
                    ViewBag.Message = "ফসলটির বর্ণনায় কোন পরিবর্তন হয়নি";
                }
            }
            ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
            return View(crop);
        }

        public ActionResult EditAdmin(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Crop crop = cropBll.GetById(id);
            if (crop == null)
            {
                return RedirectToAction("Error", "Home");
            }
            ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
            return View(crop);
        }

        // POST: Crops/Edit/5
        [HttpPost]
        public ActionResult EditAdmin(Crop crop, HttpPostedFileBase ImageFile)
        {
            if (ImageFile != null)
            {
                bool IsValidFormate = common.ImageValidation(ImageFile);
                if (IsValidFormate == false)
                {
                    ModelState.AddModelError("Image", "শুধুমাত্র jpg, png, jpeg ফরম্যাট এর ছবিগুলই গ্রহণযোগ্য ");
                    ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
                    return View(crop);
                }
                byte[] CurrentImage = common.ConvertImage(ImageFile);
                crop.Image = CurrentImage;
            }
            if (ModelState.IsValid)
            {
                status = cropBll.Edit(crop);
                if (status == true)
                {
                    return RedirectToAction("List", "Crops");
                }
                else
                {
                    ViewBag.Message = "ফসলটির বর্ণনায় কোন পরিবর্তন হয়নি";
                }
            }
            ViewBag.CropCategoryId = new SelectList(db.CropCategories, "Id", "Name");
            return View(crop);
        }
        // GET: Crops/Delete/5
        public JsonResult Delete(int id)
        {
            status = cropBll.Delete(id);

            if (status == true)
            {
                return Json(1);
            }
            return Json(0);
        }
    }
}