﻿using SmartFarming.BLL;
using SmartFarming.Models;
using SmartFarming.Models.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SmartFarming.Controllers
{
    public class ChatsController : Controller
    {
        FarmDbContext db = new FarmDbContext();
        ChatBll ChatBll = new ChatBll();
        Common common = new Common();
        bool status;

        // GET: Chats
        public ActionResult QuestionList()
        {
            List<Chat> Chats = ChatBll.List();
            return View(Chats);
        }
        public ActionResult AnswerList()
        {
            List<Chat> Chats = ChatBll.List();
            return View(Chats);
        }
        public ActionResult AnswerListCommon()
        {
            List<Chat> Chats = ChatBll.List();
            return View(Chats);
        }
        public ActionResult AnswerListById(int? id)
        {
            List<Chat> Chats = ChatBll.ListById(id);
            return View(Chats);
        }

        // GET: Chats/Details/5
        //public ActionResult Details(int id)
        //{
        //    return View();
        //}

        // GET: Chats/Create
        public ActionResult Question()
        {
            return View();
        }
        // POST: Chats/Create
        [HttpPost]
        public ActionResult Question(Chat chat,HttpPostedFileBase ImageFile)
        {
            if (Session["AccountId"] != null)
            {
                chat.FarmerId = Convert.ToInt32(Session["AccountId"].ToString());
            }
            else
            {
                RedirectToAction("LogIn", "Accounts");
            }
            if (ImageFile == null)
            {
                ModelState.AddModelError("Image", "দয়া করে একটি ছবি আপলোড করুন");
                return View(chat);
            }

            bool IsValidFormat = common.ImageValidation(ImageFile);

            if (IsValidFormat == false)
            {
                ModelState.AddModelError("Image", "শুধুমাত্র jpg, png, jpeg ফরম্যাট এর ছবিগুলই গ্রহণযোগ্য ");
                return View(chat);
            }
            byte[] convertedImage = common.ConvertImage(ImageFile);
            chat.Image = convertedImage;

            if (chat.Question!="" && chat.FarmerId!=null)
            {
                status = ChatBll.Create(chat);
                if (status == true)
                {
                    return RedirectToAction("AnswerList");
                }
                else
                {
                    ViewBag.Message = "আপনার প্রশ্ন যুক্ত হয়নি";
                }
            }
            return View(chat);
        }
        // GET: Chats/Answer/5
        public ActionResult Answer(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Chat chat = ChatBll.GetById(id);
            if (chat == null)
            {
                return RedirectToAction("Error", "Home");
            }
            return View(chat);
        }

        // POST: Chats/Answer/5
        [HttpPost]
        public ActionResult Answer(Chat chat)
        {
            if (Session["AccountId"] != null)
            {
                chat.OfficerId = Convert.ToInt32(Session["AccountId"].ToString());
            }
            else
            {
                RedirectToAction("LogIn", "Accounts");
            }
            if (chat.OfficerId!=null && chat.Answer!="")
            {
                status = ChatBll.Edit(chat);
                if (status == true)
                {
                    return RedirectToAction("QuestionList", "Chats");
                }
                else
                {
                    ViewBag.Message = "আপনার উত্তর যুক্ত হয়নি";
                }
            }
            return View(chat);
        }

        public ActionResult QuestionEdit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Chat chat = ChatBll.GetById(id);
            if (chat == null)
            {
                return RedirectToAction("Error", "Home");
            }
            return View(chat);
        }

        // POST: Chats/Edit/5
        [HttpPost]
        public ActionResult QuestionEdit(Chat chat,HttpPostedFileBase ImageFile)
        {
            if (Session["AccountId"] != null)
            {
                chat.FarmerId = Convert.ToInt32(Session["AccountId"].ToString());
            }
            else
            {
                RedirectToAction("LogIn", "Accounts");
            }
            if (ImageFile != null)
            {
                bool IsValidFormate = common.ImageValidation(ImageFile);
                if (IsValidFormate == false)
                {
                    ModelState.AddModelError("Image", "শুধুমাত্র jpg, png, jpeg ফরম্যাট এর ছবিগুলই গ্রহণযোগ্য ");
                    return View(chat);
                }
                byte[] CurrentImage = common.ConvertImage(ImageFile);
                chat.Image = CurrentImage;
            }
            if (chat.Question!=""&&chat.Date!=null)
            {
                status = ChatBll.Edit(chat);
                if (status == true)
                {
                    return RedirectToAction("AnswerList", "Chats");
                }
                else
                {
                    ViewBag.Message = "আপনার প্রশ্ন পরিবর্তিত হয়নি";
                }
            }
            return View(chat);
        }

        public ActionResult AnswerEdit(int? id)
        {
            if (id == null)
            {
                return RedirectToAction("Error", "Home");
            }
            Chat chat = ChatBll.GetById(id);
            if (chat == null)
            {
                return RedirectToAction("Error", "Home");
            }
            return View(chat);
        }
        // POST: Chats/Edit/5
        [HttpPost]
        public ActionResult AnswerEdit(Chat chat)
        {
            if (Session["AccountId"] != null)
            {
                chat.OfficerId = Convert.ToInt32(Session["AccountId"].ToString());
            }
            else
            {
                RedirectToAction("LogIn", "Accounts");
            }

            if (chat.Answer!="")
            {
                status = ChatBll.Edit(chat);
                if (status == true)
                {
                    return RedirectToAction("QuestionList", "Chats");
                }
                else
                {
                    ViewBag.Message = "আপনার উত্তর পরিবর্তিত হয়নি";
                }
            }
            return View(chat);
        }
        // GET: Chats/Delete/5
        public JsonResult Delete(int id)
        {
            status = ChatBll.Delete(id);
            if (status == true)
            {
                return Json(1);
            }
            return Json(0);
        }
    }
}