﻿using SmartFarming.DAL;
using SmartFarming.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartFarming.BLL
{
    public class CropCategoryBll
    {
        CropCategoryDal cropCategoryDal = new CropCategoryDal();
        bool status;

        internal List<CropCategory> List()
        {
            List<CropCategory> CropCategories = cropCategoryDal.List();

            return CropCategories;
        }

        internal bool Create(CropCategory cropCategory)
        {
            status = cropCategoryDal.Create(cropCategory);

            return status;
        }

        internal bool Delete(int id)
        {
            status = cropCategoryDal.Delete(id);

            return status;
        }

        internal CropCategory GetById(int? id)
        {
            CropCategory cropCategory = cropCategoryDal.GetById(id);
            return cropCategory;
        }

        internal bool Edit(CropCategory cropCategory)
        {
            status = cropCategoryDal.Edit(cropCategory);
            return status;
        }
    }
}