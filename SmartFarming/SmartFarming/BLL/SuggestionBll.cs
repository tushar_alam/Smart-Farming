﻿using SmartFarming.DAL;
using SmartFarming.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartFarming.BLL
{
    public class SuggestionBll
    {
        SuggestionDal suggestionDal = new SuggestionDal();
        bool status;

        internal List<Suggestion> List()
        {
            List<Suggestion> Suggestions = suggestionDal.List();
            return Suggestions;
        }

        internal bool Create(Suggestion suggestion)
        {
            status = suggestionDal.Create(suggestion);
            return status;
        }

        internal bool Delete(int id)
        {
            status = suggestionDal.Delete(id);
            return status;
        }

        internal Suggestion GetById(int? id)
        {
            Suggestion suggestion = suggestionDal.GetById(id);
            return suggestion;
        }

        internal bool Edit(Suggestion suggestion)
        {
            status = suggestionDal.Edit(suggestion);
            return status;
        }
    }
}