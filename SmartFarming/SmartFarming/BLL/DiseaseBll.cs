﻿using SmartFarming.DAL;
using SmartFarming.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartFarming.BLL
{
    public class DiseaseBll
    {
        DiseaseDal diseaseDal = new DiseaseDal();
        bool status;

        internal List<Disease> List()
        {
            List<Disease> Diseases = diseaseDal.List();
            return Diseases;
        }

        internal bool Create(Disease disease)
        {
            status = diseaseDal.Create(disease);
            return status;
        }

        internal bool Delete(int id)
        {
            status = diseaseDal.Delete(id);
            return status;
        }

        internal Disease GetById(int? id)
        {
            Disease disease = diseaseDal.GetById(id);
            return disease;
        }

        internal bool Edit(Disease disease)
        {
            status = diseaseDal.Edit(disease);
            return status;
        }
    }
}