﻿using SmartFarming.DAL;
using SmartFarming.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartFarming.BLL
{
    public class CropBll
    {
        CropDal cropDal = new CropDal();
        bool status;

        internal List<Crop> List()
        {
            List<Crop> Crops = cropDal.List();

            return Crops;
        }

        internal bool Create(Crop crop)
        {
            status = cropDal.Create(crop);
            return status;
        }

        internal bool Delete(int id)
        {
            status = cropDal.Delete(id);
            return status;
        }

        internal Crop GetById(int? id)
        {
            Crop crop = cropDal.GetById(id);
            return crop;
        }

        internal bool Edit(Crop crop)
        {
            status = cropDal.Edit(crop);
            return status;
        }

        internal object GetCropByCategoryId(int id)
        {
            List<Crop> Crops = cropDal.GetCropByCategoryId(id);
            return Crops;
        }
    }
}