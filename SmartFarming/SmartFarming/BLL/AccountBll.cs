﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SmartFarming.DAL;
using SmartFarming.Models;
using SmartFarming.Models.ViewModels;

namespace SmartFarming.BLL
{
    public class AccountBll
    {
        bool status = false;
        bool isExist = false;
        AccountDal accountDal = new AccountDal();
        Account account = new Account();
        internal Account Registration(RegistrationVm registrationVm)
        {
            account.Name = registrationVm.FullName; 
            account.EmailOrPhone = registrationVm.EmailOrPhone;
            account.Password = registrationVm.Pass;
            account.Gander = registrationVm.Gender;
            account.Profession = registrationVm.Occupation;
            account.DateOfBirth = registrationVm.BirthDay;

            account = accountDal.Registration(account);
            return account;
        }
        internal Account GetAccount(LoginVm loginVm)
        {
            account = accountDal.GetAccount(loginVm);
            return account;
        }
        internal bool CheckExistance(RegistrationVm registrationVm)
        {
            isExist = accountDal.CheckExistance(registrationVm);
            return isExist;
        }

        internal List<Account> List()
        {
            List<Account> Accounts = accountDal.List();
            return Accounts;
        }

        internal bool Create(Account account)
        {
            status = accountDal.Create(account);
            return status;
        }

        internal bool Delete(int id)
        {
            status = accountDal.Delete(id);
            return status;
        }

        internal Account GetById(int? id)
        {
            Account Account = accountDal.GetById(id);
            return Account;
        }

        internal bool Edit(Account account)
        {
            status = accountDal.Edit(account);
            return status;
        }
    }
}