﻿using SmartFarming.DAL;
using SmartFarming.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartFarming.BLL
{
    public class ChatBll
    {
        ChatDal chatDal = new ChatDal();
        bool status;

        internal List<Chat> List()
        {
            List<Chat> Chats = chatDal.List();
            return Chats;
        }

        internal bool Create(Chat crop)
        {
            status = chatDal.Create(crop);
            return status;
        }

        internal bool Delete(int id)
        {
            status = chatDal.Delete(id);
            return status;
        }

        internal Chat GetById(int? id)
        {
            Chat crop = chatDal.GetById(id);
            return crop;
        }

        internal bool Edit(Chat crop)
        {
            status = chatDal.Edit(crop);
            return status;
        }

        internal List<Chat> ListById(int? id)
        {
            List<Chat> Chats = chatDal.ListById(id);
            return Chats;
        }
    }
}